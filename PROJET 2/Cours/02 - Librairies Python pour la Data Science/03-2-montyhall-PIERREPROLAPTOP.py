import numpy as np
from enum import Enum


class Strategy(Enum):
    CHANGE = 1
    KEEP = 2


def play(strategy, games=1):
    '''
    :param strategy: Keep or Change Enum value
    :param games: total number of games to run
    :return: array with boolean whether the player won (True) or lost (False)
    '''
    good_doors = np.random.randint(0, 3, size=games)
    user_doors = np.random.randint(0, 3, size=games)
    return np.equal(good_doors, user_doors) if strategy == Strategy.KEEP else np.not_equal(good_doors, user_doors)

