import re
import seaborn as sns
import matplotlib.pyplot as plt
from matplotlib.patches import Circle
import numpy as np
import statsmodels.api as sm
import statsmodels.formula.api as smf
import pandas as pd
from sklearn import feature_selection
from sklearn.model_selection import train_test_split
from sklearn.model_selection import KFold
from sklearn import neighbors

NUTRIGRADE_COLORS = {
    'A': 'forestgreen',
    'B': 'lawngreen',
    'C': 'gold',
    'D': 'darkorange',
    'E': 'firebrick'
}

NUTRIGRADE_XTICKS = {
    'A': 'A [-15;0[',
    'B': 'B [0;3[',
    'C': 'C [3;11[',
    'D': 'D [11;19[',
    'E': 'E [19;40]'
}

def get_nutriscore_letter(score):
    ''' Returns the nutrigrade letter according to the numeric nutriscore '''
    if score < 0:
        return 'A'
    elif score < 3:
        return 'B'
    elif score < 11:
        return 'C'
    elif score < 19: 
        return 'D'
    else:
        return 'E'
    
def get_nutritional_columns(data, exclude_starts=[]):
    ''' Explore the data columns to return the columns names that seems to be nutritional columns '''
    if len(exclude_starts) > 0:
        regexp = '^(?!'
        for index, start in enumerate(exclude_starts):
            if index > 0:
                regexp = regexp + '|'
            regexp = regexp + '(' + start + ')'
        regexp = regexp + ').*_100g$'
    else:
        regexp = '^.*_100g$'
            
    return [x for x in data.columns.values if re.match(regexp, x)]


def get_variables_with_filling_percentage(filled_data, min=0, max=100):
    ''' Return the filled data with only a certain percentages of filling
    
        Parameters:
        filled_data (pd.series): the list of all the variables with one column containing their filling percentage
        min (int): the minimum filling percentage to keep (included)
        max (int): the maximum fulling percentage to keep (excluded except if = 100%)

        Returns:
        pd.series: the filtered variables
    '''
    # Avoid excluding 100% filled variables 
    if max == 100:
        max = 101
    plot_variables = filled_data.to_frame()
    plot_variables = plot_variables[(plot_variables[0] >= min) & (plot_variables[0] < max)]
    
    return plot_variables


def plot_variables_filling_percentage(filled_data, min=0, max=100, ax=None, color="blue"):
    ''' Plot the horizontal bar chart of the variables with a filling percentage between min and max 
        
        Parameters:
            filled_data (list) : a list containing the data name and filling percentages
            min (int) : the minimum percentage to display (included)
            max (int) : the maximum percentage to display (excluded)
            ax (plt.Axes) : the plt axe on which plot
            color (string) : the color of the bars
    '''
    if ax == None:
        ax = plt.gca()
    
    plot_variables = get_variables_with_filling_percentage(filled_data, min, max)
    
    # Plotting the results
    plot_variables.plot(kind='barh', 
                        xlim=(min,max),
                        fontsize=15,
                        ax=ax,
                        color=color)
    ax.set_title("Variables with a filling percentage between " + str(min) + "% and " + str(max) + "%", fontsize=20)
    ax.set_xlabel("Filling percentage (France data only)", fontsize=20)
    ax.tick_params(axis="y", direction="out", labelcolor=color)
    ax.invert_yaxis()
    
def extract_country_code(tag):
    ''' Extract the 2-letters country code at the beginning of a tag string
        
        Parameters:
            tag (string)
    '''
    tag_parts = tag.split(':')
    if len(tag_parts) > 1 and re.match('^[a-z]{2}$', tag_parts[0]):
        return tag_parts[0]
    return math.nan


def calculate_array_similarity_score(arr_ref, arr_comp, intersect_weight=1, surplus_weight=1):
    ''' Calculate a weighted score representing the similarity between 2 arrays
    
        Parameters:
            arr_ref (list) : the reference array (elements we want to keep)
            arr_comp (list) : the array to compare the reference with
            intersect_weight (int) : the weight of the intersection score
            surplus_weight (int) : the weight of the surplus score
    '''
    # The intersect score higher as the arr_comp contains more and more element from arr_ref
    intersect_score = len(np.intersect1d(arr_ref, arr_comp)) / len(arr_ref)
    
    # The surplus score is lower as the arr_comp contains more and more elements that are not in arr_ref
    arr_comp_cleaned = [x for x in arr_comp if x not in arr_ref]
    surplus_score = 1 - (len(arr_comp_cleaned)/len(arr_comp))

    return (intersect_score * intersect_weight + surplus_score * surplus_weight) / (intersect_weight + surplus_weight)


def plot_data_hist(data, ax=None, box_ax=None, color="blue", title=None, xlabel=None, ylabel=None, hue=None):
    ''' Plot the data histogram and add vertical lines for mean and median ; also plot a boxplot above the histogram.
        The ax parameter should be given even for a single plot (use gca())
        
        Parameters:
            data (Serie) : the data to plot as a histogram
            ax (plt.Axes) : the axe where to plot the histograml
            ax (plt.Axes) : the axe where to plot the boxplot
            color (string) : the histogram + boxplot color
            title (string) : the title of both plots
            xlabel (string) : the shared xlabel of both plots
            ylabel (string) : the ylabel of the histogram
            hue (string) : the column to use for hue coloring
    '''
    if ax == None or box_ax == None:
        fig, axes = plt.subplots(figsize=(10,7), nrows=2, sharex=True, gridspec_kw={"height_ratios": (.15, .85)})
        box_ax = axes[0]
        ax = axes[1]
    
    # Plot the boxplot of the data above the distribution histogram
    sns.boxplot(x=data, ax=box_ax, color=color)
    
    # Plot histogram and two vertical lines for mean and median
    mean = data.mean()
    median = data.median()
    hist_axes = sns.histplot(data, bins=100, ax=ax, color=color, hue=hue)
    ax.axvline(mean, color='r', linestyle='--', linewidth="2")
    ax.axvline(median, color='b', linestyle='-.', linewidth="2")
    
    box_ax.set_title(title, fontsize=16, pad=25)
    box_ax.set_xlabel("")
    
    ax.set_xlabel(xlabel, fontsize=13)
    ax.set_ylabel(ylabel, fontsize=13)
    
    skew = round(data.skew(),2)
    kurtosis = round(data.kurtosis(),2)
    
    ax.text(mean, hist_axes.get_ylim()[1]*0.95, "mean = " + str(round(mean, 2)), color='r')
    ax.text(median, hist_axes.get_ylim()[1], "med = " + str(round(median, 2)), color='b')
    ax.text(data.max()*0.8, hist_axes.get_ylim()[1]*0.91, "skewness : " + str(skew))
    ax.text(data.max()*0.8, hist_axes.get_ylim()[1]*0.85, "kurtosis : " + str(kurtosis))
    
def display_scree_plot(pca, ax, color="blue", plot_cumulative=False):
    ''' Display the PCA scree plot 
        
        Parameters:
            pca : the sklearn PCA result
            ax (plt.Axes) : the plt axe to plot the scree plot histogram
            color (string) : the color of the histogram
            plot_cumulative (boolean) : of True, also plot the cumulative line with the histogram
    '''
    scree = pca.explained_variance_ratio_ * 100
    ax.bar(np.arange(len(scree))+1, scree, color=color)
    if plot_cumulative:
        ax.plot(np.arange(len(scree))+1, scree.cumsum(), c="red", marker='o')
    ax.set_xlabel("Component number", fontsize=13)
    ax.set_ylabel("Inertia percentage", fontsize=13)
    ax.set_title("PCA Scree Plot", fontsize=16)
    
    
def display_pca_circles(pca, axis_ranks=(0,1), labels=None, label_rotation=0, lims=None, ax=None):
    ''' Display the PCA circle with arrows variables
    
        Parameters:
            pca : the PCA results
            axis_ranks (tuple) : the two components to draw on the circle
            labels : the variables labels
            label_rotation : how to rotate labels
            lims (tuple) : the plot limits
            ax (plt.Axes) : the axe where to plot the circle
    '''
    pcs = pca.components_
    d1 = axis_ranks[0]
    d2 = axis_ranks[1]

    if ax == None:
        fig, ax = plt.subplots(figsize=(7,6))

    # Determine graph limits
    if lims is not None :
        xmin, xmax, ymin, ymax = lims
    elif pcs.shape[1] < 30 :
        xmin, xmax, ymin, ymax = -1, 1, -1, 1
    else :
        xmin, xmax, ymin, ymax = min(pcs[d1,:]), max(pcs[d1,:]), min(pcs[d2,:]), max(pcs[d2,:])
    ax.set_xlim(xmin, xmax)
    ax.set_ylim(ymin, ymax)

    # Displaying arrows. If they are more than 20 we only show them as line (no peak)
    if pcs.shape[1] < 30 :
        ax.quiver(np.zeros(pcs.shape[1]), np.zeros(pcs.shape[1]),
           pcs[d1,:], pcs[d2,:], 
           angles='xy', scale_units='xy', scale=1, color="grey")
    else:
        lines = [[[0,0],[x,y]] for x,y in pcs[[d1,d2]].T]
        ax.add_collection(LineCollection(lines, axes=ax, alpha=.1, color='black'))

    # Displaying variables names
    if labels is not None:  
        for i,(x, y) in enumerate(pcs[[d1,d2]].T):
            if x >= xmin and x <= xmax and y >= ymin and y <= ymax :
                ax.text(x, y, labels[i], fontsize='14', ha='center', va='center', rotation=label_rotation, color="blue", alpha=0.5)

    # Plotting circle
    circle = Circle((0,0), radius=1, fill=False, facecolor='none', edgecolor='b')
    ax.add_patch(circle)

    # Plot middle vertical and horizontal lines
    ax.plot([-1, 1], [0, 0], color='grey', ls='--')
    ax.plot([0, 0], [-1, 1], color='grey', ls='--')

    # Axes names
    ax.set_xlabel('F{} ({}%)'.format(d1+1, round(100*pca.explained_variance_ratio_[d1],1)), fontsize=13)
    ax.set_ylabel('F{} ({}%)'.format(d2+1, round(100*pca.explained_variance_ratio_[d2],1)), fontsize=13)
    ax.set_title("Correlation circle (F{} et F{})".format(d1+1, d2+1), fontsize=16)
    

def display_factorial_planes(X_projected, pca, axis_ranks=(0,1), labels=None, alpha=1, illustrative_var=None, ax=None):
    ''' Display the projected data on the given PCA components plan '''
    d1 = axis_ranks[0]
    d2 = axis_ranks[1]

    if ax == None:
        fig = plt.figure(figsize=(15,15*len(axis_ranks)))
        ax = plt.gca()

    # affichage des points
    if illustrative_var is None:
        ax.scatter(X_projected[:, d1], X_projected[:, d2], alpha=alpha)
    else:
        illustrative_var = np.array(illustrative_var)
        for value in np.unique(illustrative_var):
            selected = np.where(illustrative_var == value)
            ax.scatter(X_projected[selected, d1], X_projected[selected, d2], alpha=alpha, label=value)
        ax.legend()

    # affichage des labels des points
    if labels is not None:
        for i,(x,y) in enumerate(X_projected[:,[d1,d2]]):
            ax.text(x, y, labels[i], fontsize='11', ha='center',va='center') 

    # détermination des limites du graphique
    boundary = np.max(np.abs(X_projected[:, [d1,d2]])) * 1.1
    ax.set_xlim([-boundary,boundary])
    ax.set_ylim([-boundary,boundary])

    # affichage des lignes horizontales et verticales
    ax.plot([-100, 100], [0, 0], color='grey', ls='--')
    ax.plot([0, 0], [-100, 100], color='grey', ls='--')

    # nom des axes, avec le pourcentage d'inertie expliqué
    ax.set_xlabel('F{} ({}%)'.format(d1+1, round(100*pca.explained_variance_ratio_[d1],1)))
    ax.set_ylabel('F{} ({}%)'.format(d2+1, round(100*pca.explained_variance_ratio_[d2],1)))

    ax.set_title("Projection des individus (sur F{} et F{})".format(d1+1, d2+1))

    
def plot_quantile_regression(data, X_column, y_column, ax=None):
    ''' Plot the quantile regression lines
    
        Parameters:
            data (DataFrame) : the data containing the columns for the quantile regression
            X_column (string) : name of the column for the X axis
            y_column (string) : name of the column for the y axis
            ax (plt.Axes) : the ax to plot the lines
    '''
    if ax == None:
        fig, ax = plt.subplots(figsize=(8, 6))
        
    mod = smf.quantreg(str(y_column) + ' ~ ' + str(X_column), data)
    res = mod.fit(q=.5)
    quantiles = [0.2, 0.4, 0.6, 0.8]
    q_colors = ['darkblue', 'green', 'darkorange', 'darkviolet']
    
    models = []
    for q in quantiles:
        res = mod.fit(q=q)
        models.append([q, res.params['Intercept'], res.params[X_column]] + \
                res.conf_int().loc[X_column].tolist())
        
    models = pd.DataFrame(models, columns=['q', 'a', 'b', 'lb', 'ub'])

    ols = smf.ols(str(y_column) + ' ~ ' + str(X_column), data).fit()
    ols_ci = ols.conf_int().loc[X_column].tolist()
    ols = dict(a = ols.params['Intercept'],
               b = ols.params[X_column],
               lb = ols_ci[0],
               ub = ols_ci[1])
    
    x = np.arange(data[X_column].min(), data[X_column].max(), 1)
    get_y = lambda a, b: a + b * x

    for i in range(models.shape[0]):
        y = get_y(models.a[i], models.b[i])
        ax.plot(x, y, linestyle='dotted', linewidth=2, color=q_colors[i], label="Q " + str(round(quantiles[i] * 100)))

    # Plotting the mediam line
    y = get_y(ols['a'], ols['b'])
    ax.plot(x, y, color='red', label='Median')
    legend = ax.legend()
    
def convert_nutrigrade_to_numeric(nutrigrade_letter):
    ''' Convert nutrigrade letter A-E todigit 0-4 '''
    letters = ['A', 'B', 'C', 'D', 'E']
    if nutrigrade_letter in letters:
        return letters.index(nutrigrade_letter)
    else:
        return np.nan

def get_knn_data_sample(data, sample_size=100):
    ''' Use the data to return a random sample split into input and output data for nutrigrade KNN calculation 
    
        Parameters:
            data (DataFrame): the initial dataset
            sample_size (int): the percentage of the initial dataset to keep
    '''
    # We exclude the fiber column as its not filled enough (20% versus 75% for the other ones) then keep only fully filled rows
    knn_inputs = data[get_nutritional_columns(data, exclude_starts=['fiber', 'nutrition'])+['nutriscore_grade']].dropna(how="any")
    
    # Converting nutrigrade letter into a numeric value for the KNN
    knn_inputs['nutriscore_grade'] = knn_inputs['nutriscore_grade'].apply(convert_nutrigrade_to_numeric)
    
    # Keeping only a sample of data
    knn_inputs = knn_inputs.sample(round(len(knn_inputs)*sample_size/100))
    knn_outputs = knn_inputs['nutriscore_grade']
    knn_inputs = knn_inputs.drop(columns='nutriscore_grade')
    
    return (knn_inputs, knn_outputs)

def plot_knn_errors(data, sample_size=100, k_folds=5, k_min=2, k_max=5, ax=None, title=None, color="blue", weights="uniform", metric="minkowski", p=1, plot_details=True):
    ''' Try multiple parameters on a K-NN and print to result errors for each value of K
    
        Parameters:
            data (DataFrame): the dataset
            sample_size (int): the size of the sample for KNN calculation (100 = full dataset)
            k_folds (int): number of kfolds to create for calculations
            k_min (int): minimum K value to test
            k_max (int): maximum K value to test
            ax (plt.Axes): figure axe where to plot the error chart
            title (string): title of the figure
            color (string): color of the mean plot line
            weights (string): the weights parameter for sklearn KNN
            metric (string): the metric parameter for sklearn KNN
            p (int): the p value for sklearn KNN
            plot_details (boolean): if True, we also plot all the kfolds errors subplots around the mean line
    '''
    # Creating a data sample and splitting if into multiple folds 
    knn_inputs, knn_outputs = get_knn_data_sample(data, sample_size=sample_size)
    kf = KFold(n_splits=k_folds)

    k_errors = {}
    k_details = {}
    for k in np.arange(k_min, k_max+1):
        all_errors = []

        for train_index, test_index in kf.split(knn_inputs):
            X_train, X_test = knn_inputs.iloc[train_index], knn_inputs.iloc[test_index]
            y_train, y_test = knn_outputs.iloc[train_index], knn_outputs.iloc[test_index]

            knn = neighbors.KNeighborsClassifier(n_neighbors=k, weights=weights, metric=metric, p=p)
            knn_result = knn.fit(X_train, y_train)

            error = 100 * (1 - knn_result.score(X_test, y_test))
            all_errors.append(error)

        k_errors[k] = sum(all_errors) / len(all_errors)
        k_details[k] = all_errors

    if ax == None:
        fig = plt.figure()
        ax = plt.gca()
        
    if title == None:
        title = "Evolution of KNN error as a function of K"
    
    # Plotting the mean error
    ax.plot(k_errors.keys(), k_errors.values(), 'o-', color=color)
    
    # Plotting kfold details if asked
    if plot_details == True:
        detail_plot_values = []
        for k, values in k_details.items():
            for index, value in enumerate(values):
                if len(detail_plot_values) < index + 1:
                    detail_plot_values.append([])
                detail_plot_values[index].append(value)
                
        for sub_plot in detail_plot_values:
            ax.plot(k_details.keys(), sub_plot, color="grey", linewidth=0.3)
    
    ax.set_title(title, fontsize=16)
    ax.set_xlabel("k", fontsize=14)
    ax.set_ylabel("% of errors", fontsize=14)
    ax.set_xticks(np.arange(k_min, k_max+1))
    
    
def calculate_categories_tags_score(value, ref_array):
    ''' Used to calculate array similarity between tags '''
    comp_arr = value.split(',')
    
    if isinstance(comp_arr, list) and isinstance(ref_array, list):
        return calculate_array_similarity_score(ref_array, comp_arr)
    else:
        return 0

def calculate_canberra_distance(row, product_row, calculation_columns):
    ''' Calculate the Canberra distance between a reference row and another DF row
    
        Parameters:
            row (Series): the DF row on which we apply the method
            product_row (DataFrame): the one-row dataframe with which calculate the distance
            calculation_columns (list): the columns used to calculate the distance
    '''
    ref_arr = product[calculation_columns]
    comp_arr = row[calculation_columns]
    return sp.spatial.distance.canberra(ref_arr, comp_arr)


def calculate_final_score(row):
    ''' Calculate the final similarity score for each row
        
        Parameters:
            row (Series): the DF row (using apply())
    '''
    return (row['organic'] + row['canberra'] + row['tags_scoring'] + row['nutrigrade_score'])/4        
        
def find_similar_products(data, product, n_suggestions=3):
    ''' Find N similar product of the given one 
    
        Parameters:
            data (DataFrame): the dataset containing all the product 
            product (DataFrame): a DF containing one row : the choosen product
            n_suggestions: the number of similar products to suggest
    '''
    calculation_data = data.copy(deep=True)

    # Removing selected product from the calculation
    calculation_data = calculation_data.drop(index=product.index, errors='ignore')

    # Calculating the categories tags scoring of all product with the selected one
    product_tags = product['categories_tags'].str.split(',').to_list()[0]
    calculation_data['tags_scoring'] = calculation_data['categories_tags']
    calculation_data['tags_scoring'] = calculation_data[calculation_data['tags_scoring'].notnull()]['tags_scoring'].apply(calculate_categories_tags_score, args=(product_tags,))
    calculation_data.loc[calculation_data['tags_scoring'].isnull(), 'tags_scoring'] = 0.0
    calculation_data = calculation_data.sort_values('tags_scoring', ascending=False)
    
    # If the tags scoring has return some scores, we keep the highest ones ; else we work on all dataset
    if len(calculation_data[calculation_data['tags_scoring'] > 0]) > 0:
        calculation_data = calculation_data[calculation_data['tags_scoring'] > 0]
    elif pd.notnull(product.iloc[0]['main_category']):
        category = product.iloc[0]['main_category']
        calculation_data = calculation_data[calculation_data['main_category'] == category]

    # Columns used to calculate the distance between rows
    calculation_columns = get_nutritional_columns(data)

    # To calculate the distance we fill NaN with zero (to make the distance with NaN maximal)
    calculation_data[calculation_columns] = calculation_data[calculation_columns].fillna(value=0.0)

    calculation_data['canberra'] = calculation_data.apply(calculate_canberra_distance, args=(product, calculation_columns, ), axis=1)
    calculation_data = calculation_data.sort_values(by=['tags_scoring', 'canberra'], ascending=[False, True])
    
    # Setting NaN organic values as false (score = 0)
    calculation_data['organic'].fillna(False, inplace=True)

    # Converting distance as a scoring
    calculation_data['canberra'] = 1 - calculation_data['canberra'] / calculation_data['canberra'].max()

    calculation_data['score'] = calculation_data.apply(calculate_final_score, axis=1)
    calculation_data.sort_values(by='score', ascending=False, inplace=True)

    if len(calculation_data) >= n_suggestions:
        return calculation_data[:n_suggestions]
    else:
        return(calculation_data)
    
def export_result_for_display(results):
    ''' Export the results as a CSV file and radar chart images 
    
        Parameters:
            results (DataFrame): a DF containing the choosen product (first row) and the similar selection
    '''
    # Exporting the result as a CSV file
    result_data.to_csv('prototype/result.csv', sep='\t')
    
    radar_columns = get_nutritional_columns(result_data, exclude_starts=['nutrition'])
    unhealthy_columns = ['saturated-fat_100g', 'sugars_100g', 'salt_100g', 'energy-kcal_100g']

    radar_chart_data = result_data[radar_columns].copy(deep=True)
    radar_chart_data = radar_chart_data/radar_chart_data.max()
    
    # Unhealthy columns are "reverse"
    radar_chart_data[unhealthy_columns] = 1 - radar_chart_data[unhealthy_columns]

    for index, result_product in enumerate(radar_chart_data.to_dict(orient='records')):
        fig = px.line_polar(r=result_product.values(), theta=radar_columns, line_close=True)
        fig.update_traces(connectgaps=True, fill='toself')
        fig.write_image("prototype/images/radar_" + str(index) + ".jpg")