import pandas as pd
import json
import re

def merge_both_files(file_2015, file_2016):
    '''
    Merge the 2015 and 2016 files into a single one
    Use the 2016 file as the reference (because it is more recent) and add buildings from 2015
    
    Parameters:
    :param file_2015: 2015 data file
    :param file_2016: 2016 data file
    :return: DataFrame created from merge
    '''
    # Keeping only new building IDs and 2016 columns in the 2015 file
    file_2015_to_merge = file_2015[file_2015['OSEBuildingID'].isin(file_2016['OSEBuildingID'].values) == False]\
        .copy(deep=True)

    # Some features need to be re-worked before merge
    # Splitting JSON address into seperate columns
    # Initializing new address columns
    file_2015_to_merge['Latitude'] = ""
    file_2015_to_merge['Longitude'] = ""
    file_2015_to_merge['Address'] = ""
    file_2015_to_merge['ZipCode'] = ""

    for index, row in file_2015_to_merge.iterrows():
        # Deserializing json address object
        address_object = json.loads(row['Location'].replace('\'{', '{').replace('}\'', '}').replace('\'', '"'))

        # Filling new columns
        file_2015_to_merge.loc[index, 'Latitude'] = address_object['latitude']
        file_2015_to_merge.loc[index, 'Longitude'] = address_object['longitude']
        file_2015_to_merge.loc[index, 'Address'] = address_object['human_address']['address']
        file_2015_to_merge.loc[index, 'ZipCode'] = address_object['human_address']['zip']

    # Renaming columns with different names
    file_2015_to_merge = file_2015_to_merge.rename(columns={
                            'Comment': 'Comments',
                            'GHGEmissionsIntensity(kgCO2e/ft2)': 'GHGEmissionsIntensity',
                            'GHGEmissions(MetricTonsCO2e)': 'TotalGHGEmissions'})

    # Merging both files using 2016 file columns
    merged_file = pd.concat([file_2016, file_2015_to_merge.filter(items=file_2016.columns.to_list())])

    return merged_file


def group_primary_property_types(data_work):
    """
    Return the PrimaryPropertyType value counts and group all values that represent less than x% of the total
    
    Parameters:
    data (DataFrame): the date to explore 
    actions (dict): the dictionary containing actions to perform
    
    """
    actions = {
        'Low-Rise Multifamily': 'Residential',
        'Mid-Rise Multifamily': 'Residential',
        'High-Rise Multifamily': 'Residential',
        'Residence Hall': 'Residential',
        'Small- and Mid-Sized Office': 'Office',
        'Medical Office': 'Office',
        'Large Office': 'Office',
        'K-12 School': 'School',
        'University': 'School',
        'Retail Store': 'Store',
        'Supermarket / Grocery Store': 'Store',
        'Distribution Center': 'Warehouse',
        'Refrigerated Warehouse': 'Warehouse',
        'Non-Refrigerated Warehouse': 'Warehouse',
        'Self-Storage Facility': 'Warehouse',
        'Senior Care Community': 'Medical',
        'Laboratory': 'Medical',
        'Hospital': 'Medical',
        'Restaurant\n': 'Restaurant',
        'Mixed Use Property': 'Mixed',
        'Restaurant': 'Other',
    }
    
    for old_value, new_value in actions.items():
        data_work.loc[data_work['PrimaryPropertyType'] == old_value, 'PrimaryPropertyType'] = new_value

    return data_work


def add_street_type_column(data_file):
    '''
    Add a column containing the street type
    
    Parameters:
    :param data_file(DataFrame): the data to update
    
    Returns:
    :return the dataframe updates
    '''
    data_file['StreetType'] = ""
    for index, row in data_file.iterrows():
        if re.match('.*Ave((nue)|\.?).*', row['Address'], re.IGNORECASE) or re.match('.*ave?\.?$$', row['Address'], re.IGNORECASE):
            street_type = 'avenue'
        elif re.match('.*way.*', row['Address'], re.IGNORECASE):
            street_type = 'way'
        elif re.match('.*drive.*', row['Address'], re.IGNORECASE):
            street_type = 'drive'
        elif re.match('.*blvd?.*', row['Address'], re.IGNORECASE):
            street_type = 'drive'
        elif re.match('.*((place)|(\spl\.?\s?)).*', row['Address'], re.IGNORECASE):
            street_type = 'place'
        elif re.match('.*((road)|(rd\.?)).*', row['Address'], re.IGNORECASE):
            street_type = 'drive'
        else:
            street_type = 'street'

        data_file.loc[index, 'StreetType'] = street_type

    return data_file


def manage_existing_features(
    df,
    remove_residential=True, 
    remove_outliers=True
):
    '''
    Add and remove some columns according to the univariate analysis above
    :param data: the initial dataframe
    :param remove_energy_columns: remove or not all the energy columns
    :return: the treated datafile
    '''

    targets = get_targets()
    scaling_features = get_scaling_features()
    scaling_features_with_energy_star_score = get_scaling_features(with_energy_star_score=True)
    encoding_features = get_encoding_features()
    drop_nan_features = get_drop_nan_features()
    
    # we add the street type column
    df = add_street_type_column(df)
    
    # adding building age
    last_year = df['YearBuilt'].max()
    df['BuildingAge'] = last_year - df['YearBuilt']
    
    # we group some categories
    df = group_primary_property_types(df)

    # Cleaning neighborhoods
    df['Neighborhood'] = df['Neighborhood'].str.upper()
    df.loc[df['Neighborhood'] == 'DELRIDGE NEIGHBORHOODS', 'Neighborhood'] = "DELRIDGE"
    
    # There is only one "Nonresidential WA" so we merge is in "Non residential"
    if 'BuildingType' in df.columns.values:
        df.loc[df['BuildingType'] == 'Nonresidential WA', 'BuildingType'] = "NonResidential"
        
        if remove_residential:
            df = df[df['BuildingType'].str.contains('Multifamily') == False]
            df = df[df['PrimaryPropertyType'] != 'Residential']
        
    if remove_outliers:
        # Tallest building is the Columbia Center with 76 floors
        # Here we remove a chinese church whith is one of the only abberant values of the dataset
        df = df[df['NumberofFloors'] <= 76] 
        
        # Threshold to update to restrain the final dataset 
        df = df[df['PropertyGFATotal'] < 3000000]
        
        # Removing non valid targets
        df = df[df['TotalGHGEmissions'] > 0]
        
        # Removing non valid targets
        df = df[(df['TotalGHGEmissions'] > 0) & (df['SiteEnergyUse(kBtu)'] > 0)]
        
        # Removing very large buildings 
        df = df[df['LargestPropertyUseTypeGFA'] < 8000000]
        
        df = df[df['SiteEnergyUse(kBtu)'] < 200000000]

    # Filtering data to keep
    df = df.filter(items=scaling_features_with_energy_star_score + encoding_features + targets, axis='columns')
    
    return df


def get_targets():
    '''
    Returns the targets names in a list
    '''
    return ['SiteEnergyUse(kBtu)', 'TotalGHGEmissions']
    
    
    
def get_scaling_features(with_energy_star_score=False):
    '''
    Returns a list with the names of features that should be scaled during preprocessing
    '''
    features = [
        'BuildingAge',
        'NumberofBuildings',
        'NumberofFloors',
        'PropertyGFATotal',
        'PropertyGFAParking',
        'LargestPropertyUseTypeGFA',
        'SecondLargestPropertyUseTypeGFA',
        'ThirdLargestPropertyUseTypeGFA'
    ]
    
    if with_energy_star_score:
        return features + ['ENERGYSTARScore']
    else:
        return features
    
    
def get_encoding_features():
    '''
    Returns the list of features that should be encoded in during preprocessing
    '''
    return [
        'PrimaryPropertyType',
        'Neighborhood',
        'SecondLargestPropertyUseType',
        'ThirdLargestPropertyUseType',
        'StreetType'
    ]


def get_drop_nan_features():
    '''
    Returns the features for which we drop all rows containing at least one NaN
    '''
    return [
        'BuildingAge',
        'NumberofBuildings',
        'NumberofFloors',
        'PropertyGFATotal',
        'PropertyGFAParking',
        'PrimaryPropertyType',
        'Neighborhood',
        'StreetType',
    ] + get_targets()


def get_merged_data():
    '''
    Merge the 2 data files and returns the merge one

    :return DataFrame containing the full merged dataset
    '''
    data_2015 = pd.read_csv('./data/2015-building-energy-benchmarking.csv', sep=',')
    data_2016 = pd.read_csv('./data/2016-building-energy-benchmarking.csv', sep=',')
    return merge_both_files(data_2015, data_2016)