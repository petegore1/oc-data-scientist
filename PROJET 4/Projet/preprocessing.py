import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import json
import re

from scipy.stats import chi2_contingency

# Preprocessing librairies
from sklearn.pipeline import Pipeline
from sklearn.compose import ColumnTransformer
from sklearn.compose import make_column_selector
from sklearn.preprocessing import FunctionTransformer
from sklearn.preprocessing import PowerTransformer
from sklearn.preprocessing import StandardScaler
from sklearn.preprocessing import RobustScaler
from sklearn.preprocessing import MinMaxScaler
from sklearn.preprocessing import OneHotEncoder

# Modeling librairies
from sklearn.base import clone
from sklearn.model_selection import train_test_split
from sklearn.model_selection import KFold
from sklearn.model_selection import GridSearchCV
from sklearn.model_selection import validation_curve
from sklearn.linear_model import LinearRegression
from sklearn.linear_model import Lasso
from sklearn.linear_model import Ridge
from sklearn.ensemble import RandomForestRegressor
from sklearn.ensemble import BaggingRegressor
from sklearn.ensemble import AdaBoostRegressor
from sklearn.ensemble import GradientBoostingRegressor
from sklearn.ensemble import VotingRegressor
from sklearn.svm import SVR

from sklearn.metrics import mean_squared_error
from sklearn.metrics import r2_score



def merge_both_files(file_2015, file_2016):
    '''
    Merge the 2015 and 2016 files into a single one
    Use the 2016 file as the reference (because it is more recent) and add buildings from 2015
    :param file_2015: 2015 data file
    :param file_2016: 2016 data file
    :return: DataFrame created from merge
    '''
    # Keeping only new building IDs and 2016 columns in the 2015 file
    file_2015_to_merge = file_2015[file_2015['OSEBuildingID'].isin(file_2016['OSEBuildingID'].values) == False]\
        .copy(deep=True)

    # Some features need to be re-worked before merge
    # Splitting JSON address into seperate columns
    # Initializing new address columns
    file_2015_to_merge['Latitude'] = ""
    file_2015_to_merge['Longitude'] = ""
    file_2015_to_merge['Address'] = ""
    file_2015_to_merge['ZipCode'] = ""

    for index, row in file_2015_to_merge.iterrows():
        # Deserializing json address object
        address_object = json.loads(row['Location'].replace('\'{', '{').replace('}\'', '}').replace('\'', '"'))

        # Filling new columns
        file_2015_to_merge.loc[index, 'Latitude'] = address_object['latitude']
        file_2015_to_merge.loc[index, 'Longitude'] = address_object['longitude']
        file_2015_to_merge.loc[index, 'Address'] = address_object['human_address']['address']
        file_2015_to_merge.loc[index, 'ZipCode'] = address_object['human_address']['zip']

    # Renaming columns with different names
    file_2015_to_merge = file_2015_to_merge.rename(columns={
                            'Comment': 'Comments',
                            'GHGEmissionsIntensity(kgCO2e/ft2)': 'GHGEmissionsIntensity',
                            'GHGEmissions(MetricTonsCO2e)': 'TotalGHGEmissions'})

    # Merging both files using 2016 file columns
    merged_file = pd.concat([file_2016, file_2015_to_merge.filter(items=file_2016.columns.to_list())])

    return merged_file


def group_primary_property_types(data_work):
    """
    Return the PrimaryPropertyType value counts and group all values that represent less than x% of the total
    
    Parameters:
    data (DataFrame): the date to explore 
    actions (dict): the dictionary containing actions to perform
    
    """
    actions = {
        'Low-Rise Multifamily': 'Residential',
        'Mid-Rise Multifamily': 'Residential',
        'High-Rise Multifamily': 'Residential',
        'Residence Hall': 'Residential',
        'Small- and Mid-Sized Office': 'Office',
        'Medical Office': 'Office',
        'Large Office': 'Office',
        'K-12 School': 'School',
        'University': 'School',
        'Retail Store': 'Store',
        'Supermarket / Grocery Store': 'Store',
        'Distribution Center': 'Warehouse',
        'Refrigerated Warehouse': 'Warehouse',
        'Non-Refrigerated Warehouse': 'Warehouse',
        'Self-Storage Facility': 'Warehouse',
        'Senior Care Community': 'Medical',
        'Laboratory': 'Medical',
        'Hospital': 'Medical',
        'Restaurant\n': 'Restaurant',
        'Mixed Use Property': 'Mixed',
        'Restaurant': 'Other',
    }
    
    for old_value, new_value in actions.items():
        data_work.loc[data_work['PrimaryPropertyType'] == old_value, 'PrimaryPropertyType'] = new_value

    return data_work

def add_street_type_column(data_file):
    data_file['StreetType'] = ""
    for index, row in data_file.iterrows():
        if re.match('.*Ave((nue)|\.?).*', row['Address'], re.IGNORECASE) or re.match('.*ave?\.?$$', row['Address'], re.IGNORECASE):
            street_type = 'avenue'
        elif re.match('.*way.*', row['Address'], re.IGNORECASE):
            street_type = 'way'
        elif re.match('.*drive.*', row['Address'], re.IGNORECASE):
            street_type = 'drive'
        elif re.match('.*blvd?.*', row['Address'], re.IGNORECASE):
            street_type = 'drive'
        elif re.match('.*((place)|(\spl\.?\s?)).*', row['Address'], re.IGNORECASE):
            street_type = 'place'
        elif re.match('.*((road)|(rd\.?)).*', row['Address'], re.IGNORECASE):
            street_type = 'drive'
        else:
            street_type = 'street'

        data_file.loc[index, 'StreetType'] = street_type

    return data_file