from datetime import datetime

import folium
import matplotlib.cm as cm
import matplotlib.pyplot as plt
import math
import numpy as np
import pandas as pd
import seaborn as sns

from matplotlib.collections import LineCollection
from matplotlib.patches import Circle

from sklearn.base import clone
from sklearn.cluster import KMeans
from sklearn.decomposition import PCA
from sklearn.metrics import silhouette_samples, silhouette_score, adjusted_rand_score
from sklearn.preprocessing import StandardScaler


def show_filling_percentages(df, color='dodgerblue'):
    """ Display a barh chart to display filling percentages of the given dataframe

    Args:
        :param df: (DataFrame) the DataFrame object to inspect
        :param color: (str) the color value for the chart
    Returns:
        :return (void): Plot the chart and returns nothing
    """
    filling = 100 * df.notnull().sum(axis=0).sort_values(ascending=True) / len(df)
    fig = plt.figure(figsize=(10, 5))
    fig.suptitle("Filling percentages for products file", fontsize=16)
    plt.barh(
        y=filling.index.values,
        width=filling,
        height=0.8,
        color=color
    )
    ax = fig.gca()
    ax.set_xlabel("Filling percentage")


def show_most_use_categories(
    df,
    column_name,
    threshold=1,
    grouping_name='other',
    color='dodgerblue',
    title=''
):
    """ Plot a barh chart with the most used categories in the given data column

    Args:
        :param df: (DataFrame) the DF object to work with
        :param column_name: (str) the column name containing the categorical data
        :param threshold: (int|float) the threshold below which we group the values
        :param grouping_name: (str) the name of the grouped categories
        :param color: (str) the color of the bars
        :param title: (str) the title of the figure

    Returns:
        :return (void): Plot the chart and returns nothing
    """
    # Counting how many products by
    categories_use = df[column_name] \
        .value_counts() \
        .sort_values(ascending=False) \
        .to_frame() \
        .reset_index()

    # Finding values to group because they are used less than the chosen threshold
    cats_to_group = categories_use[categories_use[column_name] < len(df) * threshold / 100]
    cats_to_group = cats_to_group['index'].tolist()

    # Renaming the values in the original DF then recalculating the values uses
    df.loc[df[column_name].isin(cats_to_group), column_name] = grouping_name
    final_categories_use = df[column_name] \
        .value_counts() \
        .sort_values(ascending=True) \
        .to_frame()

    # Finally ploting the result
    fig = plt.figure(figsize=(15, len(final_categories_use) * 0.5))
    fig.suptitle(title, fontsize=17, y=0.9)
    plt.barh(
        y=final_categories_use.index.values,
        width=final_categories_use[column_name],
        height=0.8,
        color=color
    )
    fig.gca().set_xlabel('Number of products', fontsize=15)


def plot_all_possible_boxplots(df, ncols=2, color='dodgerblue', title=''):
    """ Plot the boxplots for all numerical columns of the given dataframe

    Args:
        :param df: (DataFrame) the dataframe to work on
        :param ncols: (int) number of columns of subplots
        :param color: (str) the color of the boxplots
        :param title: (str) the suptitle of the figure
    Returns:
        :return: (void) Plot the boxplots and returns nothing
    """
    # Extracting numerical columns names
    numerical_columns = df.dtypes\
        .where(lambda x: (x == 'int64') | (x == 'float64'))\
        .dropna()\
        .index\
        .tolist()

    # Calculating how many rows the figure will contain
    nrows = math.ceil(len(numerical_columns) / ncols)

    # Ploting all the subplots
    fig, axes = plt.subplots(figsize=(15, nrows * 3), nrows=nrows, ncols=ncols)
    fig.subplots_adjust(hspace=0.6, wspace=0.2)
    fig.suptitle(title, fontsize=18, y=0.95)

    i = 0
    for column in numerical_columns:
        if nrows > 1:
            ax = axes[math.floor(i / ncols)][i % ncols]
        else:
            ax = axes[i % ncols]
        sns.boxplot(x=df[column], ax=ax, color=color)
        i += 1


def display_geolocations_map(
    geolocated_data,
    coordinates_data,
    join_column,
    display_threshold=10,
    max_circle_size=200000,
    color='dodgerblue',
    circle_weight=1,
    initial_zoom=8,
    initial_center=None
):
    """ Generate a folium map to display customer/seller data density

    Args:
        :param geolocated_data: (DataFrame): the DF containing the data to display on the map
        :param coordinates_data: (DataFrame): the DF containing the coordinates of each place
        :param join_column: (str) the name of the "geolocated_data" to join
        :param display_threshold: (int) the number of times the geoloc data has to be used
        :param max_circle_size: (int) size of the largest circle on the map
        :param color: (str) color of all circles
        :param circle_weight: (int) weight of all circles borders
        :param initial_zoom: (int) initial zoom of the map
        :param initial_center: (list) a [lat,lng] list for the initial center of the map
    Returns:
        :return (folium.Map): the generated map
    """
    # Calculating the total use of the join column
    column_use = geolocated_data[join_column].value_counts().to_frame()

    # Merging customer cities uses and coordinates
    column_use = pd.merge(
        column_use,
        coordinates_data,
        how='left',
        left_index=True,
        right_index=True
    ).dropna(how='any')

    # Creating base map centered on the markers list
    fmap = folium.Map(
        location=initial_center,
        control_scale=True,
        zoom_start=initial_zoom,
        zoom_control=True
    )

    if initial_center is None:
        fmap.fit_bounds(column_use[['geolocation_lat', 'geolocation_lng']].to_numpy().tolist())

    max_value = column_use[join_column].max()
    for index, point in column_use.iterrows():
        if point[join_column] > display_threshold:
            folium.Circle(
                location=(point['geolocation_lat'], point['geolocation_lng']),
                popup=index,
                radius=point[join_column] * max_circle_size / max_value,
                color=color,
                fill=True,
                fill_color=color,
                weight=circle_weight
            ).add_to(fmap)

    return fmap


def create_enriched_customer_dataset(
    data_folder_path,
    start=None,
    end=None,
    study_at=None
):
    """ Merge all given dataset into a rich customer dataset for further clusterings

    Args:
        :param data_folder_path: (String) the path to the data folder
        :param start: (datetime|None) the start date for orders filtering
        :param end: (datetime|None) the end date for orders filtering
        :param study_at: (datetime|None) the study date to calculate recency, ...

    Returns:
        :return (DataFrame): the enriched customers dataset
    """
    # Retrieving files
    customers_dataset = pd.read_csv(
        data_folder_path + 'customers.csv',
        sep=',',
        header=0
    )
    orders_dataset = pd.read_csv(
        data_folder_path + 'orders.csv',
        sep=',',
        header=0,
        parse_dates=[
            'order_purchase_timestamp',
            'order_approved_at',
            'order_delivered_carrier_date',
            'order_delivered_customer_date',
            'order_estimated_delivery_date'
        ]
    )
    order_items_dataset = pd.read_csv(
        data_folder_path + 'order_items.csv',
        sep=',',
        header=0
    )
    order_reviews_dataset = pd.read_csv(
        data_folder_path + 'order_reviews.csv',
        sep=',',
        header=0,
        parse_dates=['review_creation_date']
    )
    order_payments_dataset = pd.read_csv(
        data_folder_path + 'order_payments.csv',
        sep=',',
        header=0
    )
    products_dataset = pd.read_csv(data_folder_path + 'products.csv', sep=',', header=0)
    categories_dataset = pd.read_csv(
        data_folder_path + 'category_translations.csv',
        sep=',',
        header=0
    )

    # Setting default dates
    if start is None:
        start = orders_dataset['order_purchase_timestamp'].min()
    if end is None:
        end = orders_dataset['order_purchase_timestamp'].max()
    if study_at is None:
        study_at = datetime.now()

    # Adding the customer unique id on the order dataset
    orders_dataset = orders_dataset.merge(
        customers_dataset[['customer_id', 'customer_unique_id']],
        how='left',
        left_on='customer_id',
        right_on='customer_id'
    )

    # Adding the order main product category
    categories_data = orders_dataset.merge(
        order_items_dataset,
        how='left',
        left_on='order_id',
        right_on='order_id'
    ).merge(
        products_dataset,
        how='left',
        left_on='product_id',
        right_on='product_id'
    ).merge(
        categories_dataset,
        how='left',
        left_on='product_category_name',
        right_on='product_category_name'
    )
    main_categories = categories_data[['customer_unique_id', 'product_category_name_english']]\
        .groupby('customer_unique_id')\
        .agg({'product_category_name_english': [pd.Series.mode, 'count']})
    main_categories.columns = ['main_bought_category', 'total_bought_categories']

    # Calculating the total order price for each order
    orders_prices = order_items_dataset[['order_id', 'price']].groupby('order_id').sum()
    orders_dataset = orders_dataset.merge(
        orders_prices,
        how='left',
        left_on='order_id',
        right_on='order_id'
    )

    # Calculating the review count and mean for each order
    orders_reviews = order_reviews_dataset[['order_id', 'review_score']].groupby('order_id').agg([
        'mean', 'count'
    ])
    orders_reviews.columns = ['mean_review', 'count_reviews']
    orders_dataset = orders_dataset.merge(
        orders_reviews,
        how='left',
        left_on='order_id',
        right_on='order_id'
    )

    # Calculating number of payments for each order
    payments_data = order_payments_dataset[['order_id', 'payment_sequential']] \
        .groupby('order_id') \
        .max()
    orders_dataset = orders_dataset.merge(
        payments_data,
        how='left',
        left_on='order_id',
        right_on='order_id'
    )

    # We filter the orders on the given date range and interesting features
    orders_dataset_to_merge = orders_dataset[
        (orders_dataset['order_purchase_timestamp'] >= start)
        & (orders_dataset['order_purchase_timestamp'] <= end)
        ][[
            'customer_unique_id',
            'order_purchase_timestamp',
            'price',
            'mean_review',
            'count_reviews',
            'payment_sequential'
        ]]

    # Finding first and last order dates for each customer unique ID
    # Also adding the total spent amount for each customer
    customers_orders_data = orders_dataset_to_merge \
        .groupby('customer_unique_id') \
        .agg({
            'order_purchase_timestamp': ['min', 'max', 'count'],
            'price': ['sum', 'min', 'max'],
            'mean_review': ['mean', 'min'],
            'count_reviews': ['sum'],
            'payment_sequential': ['mean']
        })
    customers_orders_data.columns = [
        'first_order_date',
        'last_order_date',
        'total_orders',
        'total_spent_amount',
        'min_spent_amount',
        'max_spent_amount',
        'mean_review_score',
        'min_review_score',
        'total_reviews',
        'mean_number_of_payments'
    ]

    # Finally merging the files into one customer file
    customers_dataset = customers_dataset.merge(
        customers_orders_data,
        how='left',
        left_on='customer_unique_id',
        right_index=True
    ).merge(
        main_categories,
        how='left',
        left_on='customer_unique_id',
        right_index=True
    )

    # Adding fields using study date like recency
    for index, row in customers_dataset.iterrows():
        customers_dataset.loc[index, 'recency'] = (study_at - row['last_order_date']).days

    return customers_dataset


def display_pca_circles(pca, axis_ranks=(0, 1), labels=None, label_rotation=0, lims=None, ax=None):
    """ Display the PCA circle with arrows variables

    Args:
        :param pca: (PCA) the PCA after the fit
        :param axis_ranks: (tuple) : the two components to draw on the circle
        :param labels: (list) the variables labels
        :param label_rotation: (list) how to rotate labels
        :param lims: (tuple) the plot limits
        :param ax: (plt.Axes) the axe where to plot the circle
    """
    pcs = pca.components_
    d1 = axis_ranks[0]
    d2 = axis_ranks[1]

    if ax is None:
        fig, ax = plt.subplots(figsize=(7, 7))

    # Determine graph limits
    if lims is not None:
        xmin, xmax, ymin, ymax = lims
    elif pcs.shape[1] < 30:
        xmin, xmax, ymin, ymax = -1, 1, -1, 1
    else:
        xmin, xmax, ymin, ymax = min(pcs[d1, :]), max(pcs[d1, :]), min(pcs[d2, :]), max(pcs[d2, :])

    ax.set_xlim(xmin, xmax)
    ax.set_ylim(ymin, ymax)

    # Displaying arrows. If they are more than 20 we only show them as line (no peak)
    if pcs.shape[1] < 30:
        ax.quiver(
            np.zeros(pcs.shape[1]),
            np.zeros(pcs.shape[1]),
            pcs[d1, :],
            pcs[d2, :],
            angles='xy',
            scale_units='xy',
            scale=1,
            color="grey"
        )
    else:
        lines = [[[0, 0], [x, y]] for x, y in pcs[[d1, d2]].T]
        ax.add_collection(LineCollection(lines, axes=ax, alpha=.1, color='black'))

    # Displaying variables names
    if labels is not None:
        for i, (x, y) in enumerate(pcs[[d1, d2]].T):
            if xmin <= x <= xmax and ymin <= y <= ymax:
                ax.text(
                    x,
                    y,
                    labels[i],
                    fontsize='14',
                    ha='center',
                    va='center',
                    rotation=label_rotation,
                    color="blue",
                    alpha=0.5
                )

    # Plotting circle
    circle = Circle((0, 0), radius=1, fill=False, facecolor='none', edgecolor='b')
    ax.add_patch(circle)

    # Plot middle vertical and horizontal lines
    ax.plot([-1, 1], [0, 0], color='grey', ls='--')
    ax.plot([0, 0], [-1, 1], color='grey', ls='--')

    # Axes names
    ax.set_xlabel(
        'F{} ({}%)'.format(d1+1, round(100*pca.explained_variance_ratio_[d1], 1)),
        fontsize=13
    )
    ax.set_ylabel(
        'F{} ({}%)'.format(d2+1, round(100*pca.explained_variance_ratio_[d2], 1)),
        fontsize=13
    )
    ax.set_title("Correlation circle (F{} et F{})".format(d1+1, d2+1), fontsize=16)


def run_pca_and_scaling(data, n_components=2):
    """ Run a PCA after scaling the data

    Args:
        :param data: (DataFrame) the DF containing the customer
        :param n_components: (int) number of components for the PCA

    Returns:
        :return: the PCA, the transformed data and the list of features used for the PCA
    """
    # First of all we need to filter our features to keep only the numerical ones
    numerical_features = data.dtypes
    numerical_features = numerical_features \
        .where((numerical_features == 'int64') | (numerical_features == 'float64')) \
        .dropna() \
        .index \
        .tolist()
    numerical_features.remove('customer_zip_code_prefix')

    # Filtering the customers dataset
    data_pca = data[numerical_features].dropna(how="any")

    # Scaling the data before the PCA
    scaler = StandardScaler()
    scaled_data = scaler.fit_transform(data_pca)

    # Finally running the PCA and returning the awaited results
    pca = PCA(n_components=n_components)
    transformed_data = pca.fit_transform(scaled_data)

    return pca, transformed_data, numerical_features


def plot_silhouette(kmeans_model, data, ax, n_clusters):
    """ Plot a silhouette chart using the given data

    Args:
        :param kmeans_model: (KMeans)
        :param data: (DataFrame) the data to use to train the model
        :param ax: (plt.Axes) the ax where to plot the chart
        :param n_clusters: (int) the number of cluster of the given model
    """
    cluster_labels = kmeans_model.fit_predict(data)
    silhouette_avg = silhouette_score(data, cluster_labels)
    sample_silhouette_values = silhouette_samples(data, cluster_labels)

    y_lower = 10
    for i in range(n_clusters):
        # Aggregate the silhouette scores for samples belonging to cluster i, and sort them
        ith_cluster_silhouette_values = sample_silhouette_values[cluster_labels == i]
        ith_cluster_silhouette_values.sort()
        size_cluster_i = ith_cluster_silhouette_values.shape[0]
        y_upper = y_lower + size_cluster_i

        color = cm.nipy_spectral(float(i) / n_clusters)
        ax.fill_betweenx(
            np.arange(y_lower, y_upper),
            0,
            ith_cluster_silhouette_values,
            facecolor=color,
            edgecolor=color,
            alpha=0.7,
        )

        # Label the silhouette plots with their cluster numbers at the middle
        ax.text(-0.05, y_lower + 0.5 * size_cluster_i, str(i))

        # Compute the new y_lower for next plot
        y_lower = y_upper + 10  # 10 for the 0 samples

    ax.set_xlim(None, 1)
    ax.set_title(
        "Silhouette plot for {} clusters".format(n_clusters),
        fontsize=14
    )
    ax.set_xlabel("The silhouette coefficient values")
    ax.set_ylabel("Cluster label")
    ax.axvline(x=silhouette_avg, color="red", linestyle="--")


def plot_multiple_silhouette_scores(data_train, data_predict, clusters):
    """ Plot multiple silhouette scores side by side to compare them

    Args:
        :param data_train: (DataFrame) the data to which apply the KMeans
        :param data_predict: (DataFrame) the data to which apply the KMeans
        :param clusters: (list) the list of number of clusters

    Returns:
        :return: (list) The list of all average silhouette scores
    """
    plot_per_line = 2
    silhouette_scores = []

    # Preparing figure
    nrows = math.ceil(len(clusters) / plot_per_line)
    fig, axes = plt.subplots(figsize=(15, 9 * nrows), nrows=nrows, ncols=plot_per_line)
    fig.subplots_adjust(hspace=0.3, wspace=0.2)

    index = 0
    for n_cluster in clusters:
        if nrows > 1:
            ax = axes[math.floor(index / plot_per_line)][index % plot_per_line]
        else:
            ax = axes[index]

        # Running KMeans and calculating silhouette scores
        kmeans = KMeans(n_clusters=n_cluster, n_init=30, random_state=10)
        kmeans.fit(data_train)
        cluster_labels = kmeans.predict(data_predict)
        silhouette_avg = silhouette_score(data_predict, cluster_labels)
        sample_silhouette_values = silhouette_samples(data_predict, cluster_labels)
        silhouette_scores.append(silhouette_avg)

        y_lower = 10
        for i in range(n_cluster):
            # Aggregate the silhouette scores for samples belonging to cluster i, and sort them
            ith_cluster_silhouette_values = sample_silhouette_values[cluster_labels == i]
            ith_cluster_silhouette_values.sort()
            size_cluster_i = ith_cluster_silhouette_values.shape[0]
            y_upper = y_lower + size_cluster_i

            color = cm.nipy_spectral(float(i) / n_cluster)
            ax.fill_betweenx(
                np.arange(y_lower, y_upper),
                0,
                ith_cluster_silhouette_values,
                facecolor=color,
                edgecolor=color,
                alpha=0.7,
            )

            # Label the silhouette plots with their cluster numbers at the middle
            ax.text(-0.05, y_lower + 0.5 * size_cluster_i, str(i))

            # Compute the new y_lower for next plot
            y_lower = y_upper + 10  # 10 for the 0 samples

        ax.set_xlim(None, 1)
        ax.set_title("Silhouette plot for {} clusters".format(n_cluster), fontsize=14)
        ax.set_xlabel("The silhouette coefficient values")
        ax.set_ylabel("Cluster label")
        ax.axvline(x=silhouette_avg, color="red", linestyle="--")
        index += 1

    return silhouette_scores


def calculate_ari_score(customers_ari, model, scaler=None, min_recency=0, limit=60000):
    """ Use the given customer dataset and return 2 customers dataset based on recency

    Args:
        :param customers_ari: (DataFrame) the initial customers dataset
        :param model: (model) the model to use for the clustering
        :param scaler: (scaler|None) the scaler to apply to the data
        :param min_recency: (int) the minimum recency for the second dataset (included)
        :param limit: (int) keep the last X customers to avoid sklearn ARI issue with big datasets

    Returns:
        :return: (float) the ARI score between the two dataset clusters
    """
    # Cloning the model to have one model per dataset
    model_older = clone(model)

    # We sort the customers by recency and keep only the X first one
    # It can be used to avoid sklearn bug on ARI calculation on big datasets
    customers_ari.sort_values(by='recency', ascending=True, inplace=True)
    customers_ari = customers_ari.head(limit)

    # We create a second dataset with older customer (recency > Y) and we reset the recency
    older_customers = customers_ari[customers_ari['recency'] >= min_recency].copy(deep=True)
    older_customers['recency'] = older_customers['recency'] - older_customers['recency'].min()

    if scaler is not None:
        customers_ari = scaler.fit_transform(customers_ari)
        older_customers = scaler.fit_transform(older_customers)

    # Training the two models
    model.fit(customers_ari)
    model_older.fit(older_customers)

    # Predicting the whole dataset before comparison
    labels_all = model.predict(customers_ari)
    labels_older = model_older.predict(customers_ari)

    ari_score = adjusted_rand_score(labels_all, labels_older)

    return ari_score


def compare_clusters(data, cluster_column='Cluster'):
    """ Display plots in order to compare clusters

    Args:
        :param data: (DataFrame) the dataset containing the data and the cluster
        :param cluster_column: (string) the column containing the cluster label of each sample

    Returns:
        :return: (void) returns nothin, it only plots the charts
    """
    n_features = len(data.columns) - 1

    fig, axes = plt.subplots(
        figsize=(15, 5 * (n_features + 1)),
        ncols=2,
        nrows=n_features + 1
    )
    fig.subplots_adjust(hspace=0.3, wspace=0.2)

    # On the top of the chart we will plot the clusters sizes
    clusters_vc = data['Cluster'].value_counts().sort_index().to_frame()
    sns.barplot(
        data=clusters_vc,
        x=clusters_vc.index.tolist(),
        y='Cluster',
        palette='Accent',
        ax=axes[0][0]
    )
    axes[0][0].set_title('Clusters sizes', fontsize=14)
    axes[0][1].axis('off')

    feature_index = 1
    columns = data.columns.tolist()
    columns.remove(cluster_column)
    for feature in columns:
        # On the lest we plot the boxplots
        sns.boxplot(
            data=data,
            y=feature,
            x=cluster_column,
            ax=axes[feature_index][0],
            palette='Accent'
        )
        axes[feature_index][0].set_title(feature, fontsize=14)

        # On the right we plot the histograms
        sns.histplot(
            data=data,
            x=feature,
            element='poly',
            fill=False,
            hue=cluster_column,
            bins=100,
            ax=axes[feature_index][1],
            palette='Accent'
        )
        axes[feature_index][1].set_title(feature, fontsize=14)

        feature_index += 1

    plt.show()
