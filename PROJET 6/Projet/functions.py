import math
import matplotlib.pyplot as plt
import nltk
import numpy as np
import pandas as pd
import seaborn as sns
import string

from keras.preprocessing import image as keras_image
from keras.applications.vgg16 import preprocess_input

from matplotlib import cm

from nltk.corpus import stopwords
from nltk.stem.porter import PorterStemmer

from sklearn.cluster import KMeans
from sklearn.manifold import TSNE
from sklearn.metrics import adjusted_rand_score, silhouette_score, silhouette_samples
from sklearn.preprocessing import LabelEncoder

IMAGE_PATH = './data/Images/'
LABELS_COLORS = ["goldenrod", "fuchsia", "steelblue", "indigo", "orange", "green", "firebrick"]


def preprocess_string(strp):
    """ Prepare a string for NLP use

    Args:
        :param strp: (string) the string to preprocess

    Returns:
        :returns (string) the list of extracted tokens
    """
    # Setting everything in lowercase
    strp = strp.lower()

    # Removing punctuations
    strp = "".join([char for char in strp if char not in string.punctuation])

    # Tokeninzing
    tokens = nltk.word_tokenize(strp)
    tokens = [token for token in tokens if token not in stopwords.words('english')]

    # Stemming
    porter = PorterStemmer()
    tokens = [porter.stem(word) for word in tokens]

    return " ".join(tokens)


def prepare_dataset(data):
    """ Apply all the preprocessing operations on the initial dataset

    Args:
        :param data: (DataFrame) the initial dataset

    Returns:
        :returns (DataFrame) the new DF with all new columns and processed data
    """
    # Some columns have a default value instead of NaN but we prefer NaN
    data.loc[data['product_rating'] == 'No rating available', 'product_rating'] = np.nan
    data.loc[data['overall_rating'] == 'No rating available', 'overall_rating'] = np.nan

    # Contatenating the product name and the description into a single column
    data['full_description'] = data[['product_name', 'description']].agg(' '.join, axis=1)

    # Creating a NLP column with preprocessed preprocess strings
    data['nlp_description'] = data['full_description'].apply(preprocess_string)
    data['nlp_product_name'] = data['product_name'].apply(preprocess_string)

    # We are going to extract the different categories levels
    for index, row in data.iterrows():
        # We remove the 2 first and last chars because they just encapsulate the list
        # Then we split it using the visible separator in the column
        categories = row['product_category_tree'][2:-2].split(' >> ')

        # Now we loop in the categories to put them in a separate column each
        for category_index, category in enumerate(categories):
            column_name = "category_" + str(category_index)
            if column_name not in data.columns:
                data[column_name] = np.nan
            data.loc[index, column_name] = category

    label_encoder = LabelEncoder()
    data["label"] = label_encoder.fit_transform(data["category_0"])

    return data


def apply_kmeans(
    data,
    initial_data,
    labels_categories,
    random_state=None,
    no_plot=False,
    n_clusters=7,
):
    """ Run a KMeans on the given dataset then calculate the ARI score by comparing
    the created clusters to the initial dataset categories

    Args:
        :param data: (DataFrame) the initial dataset to apply the Kmeans on
        :param initial_data: (DataFrame) the initial dataset for merging the clusters
        :param labels_categories: (list) the list of categories names
        :param random_state: (int) the random state for the KMeans
        :param no_plot: (boolean) wether to plot or not the clusters
        :param n_clusters: (int) the number of clusters for the KMeans

    Returns:
        :returns (tuple) the ARI of the clusters and the full dataset
    """
    km = KMeans(n_clusters=n_clusters, random_state=random_state)
    cv_clusters = km.fit_predict(data)

    cluster_df = pd.DataFrame(cv_clusters)
    cluster_df.columns = ['cluster']
    data_clusters = pd.concat([initial_data, cluster_df], axis=1)

    if not no_plot:
        # Let's clusters our 1000 categories probabilities
        fig, axes = plt.subplots(figsize=(15, 24), nrows=4, ncols=2)
        fig.subplots_adjust(hspace=0.6)

        # Lets create the histogram of each cluster
        for i in data_clusters['cluster'].unique():
            data_plot = data_clusters[data_clusters['cluster'] == i]['label'].value_counts()

            kept_colors = []
            for label in data_plot.index:
                kept_colors.append(LABELS_COLORS[label])

            axes[math.floor(i / 2)][i % 2].bar(
                x=data_plot.index,
                height=data_plot,
                color=kept_colors
            )
            axes[math.floor(i / 2)][i % 2].set_title(
                "Categories distribution for cluster " + str(i)
            )
            axes[math.floor(i / 2)][i % 2].set_xticks(range(7))
            axes[math.floor(i / 2)][i % 2].set_xticklabels(
                labels_categories,
                rotation=45,
                ha="right",
                fontsize=10
            )

    ari = adjusted_rand_score(data_clusters['label'], data_clusters['cluster'])

    return ari, data_clusters


def test_vectorizer_params(
    data,
    vectorizer,
    default_params,
    variable_params,
    labels_categories,
    column='description',
    plot=True
):
    """ Run the given vectorizer with all given params and return the
    ARI scores for each try. It's not a grid-search : we change only one
    parameter at a time for smaller run times.

    Args:
        :param data: (DataFrame) the data for kmeans
        :param vectorizer: (model) the vectorizer to use
        :param default_params: (dict) the default params values
        :param variable_params: (dict) the variable params and their values
        :param labels_categories: (list) categories of the labels
        :param column: (string) the column name to use for vectorizer prediction
        :param plot: (boolean) whether to plot the clusters sizes or not

    Returns:
        :return: (list) all the calculated ARI scores
    """
    aris = {}
    for param_name, param_values in variable_params.items():
        aris[param_name] = []

        # First of all we fix all other params values with their default ones
        vectorizer_params = {}
        for name, value in default_params.items():
            if name != param_name:
                vectorizer_params[name] = value

        # Now we can loop on variable param to calculate each ARI score
        for param_val in param_values:
            vectorizer_params[param_name] = param_val
            vectorizer.set_params(**vectorizer_params)
            x = vectorizer.fit_transform(data[column])
            ari, data_clusters = apply_kmeans(
                x,
                data,
                labels_categories,
                random_state=20,
                no_plot=True
            )
            aris[param_name].append(ari)

    # Plot the aris evolution according to each param
    if plot:
        n_cols = 2
        n_rows = math.ceil(len(aris) / n_cols)
        fig, axes = plt.subplots(nrows=n_rows, ncols=n_cols, figsize=(15, 5 * n_rows))
        param_index = 0
        for param_name, param_aris in aris.items():
            ax = axes[math.floor(param_index / n_cols)][param_index % n_cols]
            ax.plot(param_aris)
            ax.set_title("ARI Evolution With Parameter " + param_name, fontsize=16)
            ax.set_xticks(range(len(variable_params[param_name])))
            ax.set_xticklabels(variable_params[param_name])
            param_index += 1

    return aris


def build_orb_histogram(kmeans, descriptors, image_num):
    """ Create the histogram of a given image

    Args:
        :param kmeans: (KMeans) the Kmeans model from sklearn
        :param descriptors: (list) the list of the image descriptors
        :param image_num: (int) the index of the image

    Returns:
        :return: (np.array) the histogram of the image
    """
    clusters = kmeans.predict(descriptors)
    hist = np.zeros(len(kmeans.cluster_centers_))
    nb_des = len(descriptors)

    if nb_des == 0:
        print("Issue with histogram for image  : ", image_num)
    for i in clusters:
        hist[i] += 1.0/nb_des

    return hist


def plot_silhouette(kmeans_model, data, ax, n_clusters):
    """ Plot a silhouette chart using the given data

    Args:
        :param kmeans_model: (KMeans)
        :param data: (DataFrame) the data to use to train the model
        :param ax: (plt.Axes) the ax where to plot the chart
        :param n_clusters: (int) the number of cluster of the given model
    """
    cluster_labels = kmeans_model.fit_predict(data)
    silhouette_avg = silhouette_score(data, cluster_labels)
    sample_silhouette_values = silhouette_samples(data, cluster_labels)

    y_lower = 10
    for i in range(n_clusters):
        # Aggregate the silhouette scores for samples belonging to cluster i, and sort them
        ith_cluster_silhouette_values = sample_silhouette_values[cluster_labels == i]
        ith_cluster_silhouette_values.sort()
        size_cluster_i = ith_cluster_silhouette_values.shape[0]
        y_upper = y_lower + size_cluster_i

        color = cm.nipy_spectral(float(i) / n_clusters)
        ax.fill_betweenx(
            np.arange(y_lower, y_upper),
            0,
            ith_cluster_silhouette_values,
            facecolor=color,
            edgecolor=color,
            alpha=0.7,
        )

        # Label the silhouette plots with their cluster numbers at the middle
        ax.text(-0.05, y_lower + 0.5 * size_cluster_i, str(i))

        # Compute the new y_lower for next plot
        y_lower = y_upper + 10  # 10 for the 0 samples

    ax.set_xlim(None, 1)
    ax.set_title(
        "Silhouette plot for {} clusters".format(n_clusters),
        fontsize=14
    )
    ax.set_xlabel("The silhouette coefficient values")
    ax.set_ylabel("Cluster label")
    ax.axvline(x=silhouette_avg, color="red", linestyle="--")


def test_keras_model(
    data,
    model,
    image_column='image',
    label_column='label',
    category_column='category_0',
    n_clusters=7,
    image_resize=(224, 224)
):
    """ Predict features using a Keras model, then use Kmeans clustering and
    finally plot the content of each cluster in order to analyse it

    Args:
        :param data: (DataFrame) the data to use for model prediction
        :param model: (model) the Keras CNN model to use
        :param image_column: (string) the data column containing the image filename
        :param label_column: (string) the data column containing the official category label
        :param category_column: (string) the data column containing the official category name
        :param n_clusters: (int) the number of cluster for the KMeans clustering
        :param image_resize: (tuple) the size for image resizing before predicting

    Returns:
        :returns: (tuple) the features list for all images and the data with a cluster column
    """
    # Retrieving the categories and labels
    labels_categories = []
    for label in range(len(data[label_column].unique())):
        labels_categories.append(data[data[label_column] == label].iloc[0][category_column])

    # Training the given Keras model to extract each image features
    images = data[image_column].to_list()
    model_feature_list = []
    for image in images:
        img = keras_image.load_img(IMAGE_PATH + image, target_size=image_resize)
        img_data = keras_image.img_to_array(img)
        img_data = np.expand_dims(img_data, axis=0)
        img_data = preprocess_input(img_data)

        model_feature = model.predict(img_data)
        model_feature_np = np.array(model_feature)
        model_feature_list.append(model_feature_np.flatten())

    # Putting all features in a single array
    model_feature_list_np = np.array(model_feature_list)

    # Training a Kmeans a predicting clusters for all our images
    kmeans = KMeans(n_clusters=n_clusters, random_state=0)
    kmeans_clusters = kmeans.fit_predict(model_feature_list_np)

    # Adding the clusters in a column into the original dataset
    cluster_df = pd.DataFrame(kmeans_clusters)
    cluster_df.columns = ['cluster']
    data_model_2 = pd.concat([data, cluster_df], axis=1)

    # Now we can plot the numbers of product categories into each cluster
    fig, axes = plt.subplots(figsize=(15, 24), nrows=4, ncols=2)
    fig.subplots_adjust(hspace=0.6)

    # Lets create the histogram of each cluster
    for i in data_model_2['cluster'].unique():
        data_plot = data_model_2[data_model_2['cluster'] == i]['label'].value_counts()

        kept_colors = []
        for label in data_plot.index:
            kept_colors.append(LABELS_COLORS[label])

        axes[math.floor(i / 2)][i % 2].bar(x=data_plot.index, height=data_plot, color=kept_colors)
        axes[math.floor(i / 2)][i % 2].set_title("Categories distribution for cluster " + str(i))
        axes[math.floor(i / 2)][i % 2].set_xticks(range(7))
        axes[math.floor(i / 2)][i % 2].set_xticklabels(
            labels_categories,
            rotation=45,
            ha="right",
            fontsize=10
        )

    # Finally we create the silhouette plot of our clusters
    plot_silhouette(
        kmeans_model=kmeans,
        data=model_feature_list_np,
        n_clusters=len(data['category_0'].unique()),
        ax=axes[3][1]
    )

    return model_feature_list_np, data_model_2


def plot_clusters_using_tsne(features, data_enriched, clusters_to_labels=None):
    """ Plot the initial labels and the clusters 2D repartition using TSNE

    Args:
        :param features: (DataFrame) the list of features (CNN output)
        :param data_enriched: (DataFrame) the initial dataset with labels and clusters columns
        :param clusters_to_labels: (list) an array with cluster -> label correspondances to keep the
        same colors on the plot

    Returns:
        :return: (void) Plots the 2 scatter figures
    """
    tsne = TSNE(
        n_components=2,
        perplexity=30,
        n_iter=2000,
        init='random',
        random_state=6
    )
    x_tsne = tsne.fit_transform(features)

    df_tsne = pd.DataFrame(x_tsne[:, 0:2], columns=['dim_1', 'dim_2'])
    df_tsne["label"] = data_enriched["label"]
    df_tsne["cluster"] = data_enriched["cluster"]

    palette = LABELS_COLORS
    if clusters_to_labels is not None:
        palette_2 = []
        for label in clusters_to_labels:
            palette_2.append(palette[label])
    else:
        palette_2 = palette

    fig, axes = plt.subplots(figsize=(12, 15), nrows=2)
    fig.subplots_adjust(hspace=0.2)
    sns.scatterplot(
        x="dim_1",
        y="dim_2",
        hue="label",
        data=df_tsne,
        legend="brief",
        palette=palette,
        s=30,
        alpha=0.6,
        ax=axes[0]
    )

    axes[0].set_title('Initial categories', fontsize=18, fontweight='bold')
    axes[0].set_xlabel('dim_1', fontsize=13, fontweight='bold')
    axes[0].set_ylabel('dim_2', fontsize=13, fontweight='bold')
    axes[0].legend(prop={'size': 14})

    sns.scatterplot(
        x="dim_1",
        y="dim_2",
        hue="cluster",
        data=df_tsne,
        legend="brief",
        palette=palette_2,
        s=30,
        alpha=0.6,
        ax=axes[1]
    )

    axes[1].set_title('Attributed clusters', fontsize=18, pad=10, fontweight='bold')
    axes[1].set_xlabel('dim_1', fontsize=13, fontweight='bold')
    axes[1].set_ylabel('dim_2', fontsize=13, fontweight='bold')
    axes[1].legend(prop={'size': 14})

    plt.show()
